import { apiResult } from '@utils/util'
const { users, usersCollection } = require('@mongo/mongo')

const getUsers = async (ctx: any) => {
    // const result = await db.users.find({}); // 查询
    // db.users.collection.find().toArray((err: any, docs: unknown) => {
    // 	if (!err) {
    // 		console.log('docs', docs);
    // 	} else {
    // 		console.log('err', err);
    // 	}
    // });
    // db.usersCollection.find().toArray((err: any, docs: unknown) => {
    // 	if (!err) {
    // 		console.log('docs', docs);next()
    // 	} else {
    // 		console.log('err', err);
    // 	}
    // });

    const res = await new Promise((resolve, reject) => {
        usersCollection.find().toArray((err: any, docs: unknown) => {
            if (!err) {
                resolve(docs);
            } else {
                reject(err);
            }
        });
    });

    ctx.body = res;
};

const findByUserName = async (ctx: any) => {
    const params = ctx.request.body;
    const result = await usersCollection.findOne(params);
    ctx.body = apiResult(result);
};

module.exports = {
    'GET /getUsers': getUsers,
    'POST /findByUserName': findByUserName
};
