const index = async (ctx: any) => {
    ctx.response.body = `<h1>Index</h1>
    <form action="/api/signin" method="post">
        <p>Name:<input name="name" value="koa"/></p>
        <p>PWD:<input name="pwd" type="password"/></p>
        <p><input type="submit" value="Submit"/></p>
    </form>
    `;
};

const signin = async (ctx: any) => {
    let data = JSON.stringify(ctx.request.body);
    // console.log('signin:',ctx.request.body);
    ctx.response.body = `<h6>data: ${data}</h6>`;
};

module.exports = {
    'GET /': index,
    'POST /signin': signin
}