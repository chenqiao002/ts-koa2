const newsDetail = async (ctx: any) => {
    let id = ctx.params.id;
    ctx.response.body = {
        "id": id,
        "title": `id:${id} Python：38行代码获取公众号下所有的文章`,
        "digest": "随着互联网的不断发展，网络上兴起了很多的自媒体平台。不用我说，相信大家也能知道当下非常流行的平台都有哪些。可以说凡是比较知名的自媒体，",
        "url": "http://mp.weixin.qq.com/s?__biz=Mzk0NjIzOTk2Mw==&amp;mid=100001083&amp;idx=1&amp;sn=e9cbbf4efb0fe71a3a61ba2a1c6ca240&amp;chksm=4308658f747fec99aa0f45a6813552f40bd56dc0a0a1d1ef502423aff0280d028fb47d52052d#rd",
        "thumb_url": "http://mmbiz.qpic.cn/mmbiz_jpg/CAkaCWmWxmLChhuVtv8CynBQRhQM1QZV4UTGUuchFXWlsuY0n0jlERjlJdUCeB6e14BDlVkKZHtxvBDbllgWzg/0?wx_fmt=jpeg"
    };
};

const newsList = async (ctx: any) => {
    ctx.response.body = {
        "news": [
            {
                "id": "001",
                "title": "Python：38行代码获取公众号下所有的文章",
                "digest": "随着互联网的不断发展，网络上兴起了很多的自媒体平台。不用我说，相信大家也能知道当下非常流行的平台都有哪些。可以说凡是比较知名的自媒体，",
                "url": "http://mp.weixin.qq.com/s?__biz=Mzk0NjIzOTk2Mw==&amp;mid=100001083&amp;idx=1&amp;sn=e9cbbf4efb0fe71a3a61ba2a1c6ca240&amp;chksm=4308658f747fec99aa0f45a6813552f40bd56dc0a0a1d1ef502423aff0280d028fb47d52052d#rd",
                "thumb_url": "http://mmbiz.qpic.cn/mmbiz_jpg/CAkaCWmWxmLChhuVtv8CynBQRhQM1QZV4UTGUuchFXWlsuY0n0jlERjlJdUCeB6e14BDlVkKZHtxvBDbllgWzg/0?wx_fmt=jpeg"
            },
            {
                "id": "002",
                "title": "Python",
                "digest": "随着互联网的",
                "url": "http://mp.weixin.qq.com/",
                "thumb_url": "http://mmbiz.qpic.cn"
            }
        ]
    };
};

const upload = async (ctx: any) => {
    // formData 文件上传数据
    // console.log(ctx.request.files);
    // formData 携带数据
    // console.log(ctx.request.body);

    ctx.response.body = {
        "title": `Python：38行代码获取公众号下所有的文章`,
        "digest": "随着互联网的不断发展，网络上兴起了很多的自媒体平台。不用我说，相信大家也能知道当下非常流行的平台都有哪些。可以说凡是比较知名的自媒体，",
        "url": "http://mp.weixin.qq.com/s?__biz=Mzk0NjIzOTk2Mw==&amp;mid=100001083&amp;idx=1&amp;sn=e9cbbf4efb0fe71a3a61ba2a1c6ca240&amp;chksm=4308658f747fec99aa0f45a6813552f40bd56dc0a0a1d1ef502423aff0280d028fb47d52052d#rd",
        "thumb_url": "http://mmbiz.qpic.cn/mmbiz_jpg/CAkaCWmWxmLChhuVtv8CynBQRhQM1QZV4UTGUuchFXWlsuY0n0jlERjlJdUCeB6e14BDlVkKZHtxvBDbllgWzg/0?wx_fmt=jpeg"
    };
};

module.exports = {
    'GET /newsDetail/:id': newsDetail,
    'GET /newsList': newsList,
    'POST /upload': upload
}
