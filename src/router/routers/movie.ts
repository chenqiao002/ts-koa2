import db from '@mongo/mongo';
// import movieModel from '@mongoose/model/movieModel';

const getMovie = async (ctx: any) => {
    const result = await db.movies.find({}); // 查询
    ctx.body = result;
};

// const getMovieList = async (ctx: any) => {
//     const result = await new Promise(function(resolve, reject){
//         movieModel.find({}, function (err, docs) {
//             if (err) {
//                 reject(err);
//                 return;
//             }
//             resolve(docs);
//         });
//     });
//     // console.log("result",result);
//     ctx.body = result;
// };

const addMovies = async (ctx: any) => {
    // const result = await DB.insert('movies', form); // 新增
    // ctx.body = result;
    // ctx.body = {
    //     "news": [
    //         {
    //             "id": "001",
    //             "title": "Python：38行代码获取公众号下所有的文章",
    //             "digest": "随着互联网的不断发展，网络上兴起了很多的自媒体平台。不用我说，相信大家也能知道当下非常流行的平台都有哪些。可以说凡是比较知名的自媒体，",
    //             "url": "http://mp.weixin.qq.com/s?__biz=Mzk0NjIzOTk2Mw==&amp;mid=100001083&amp;idx=1&amp;sn=e9cbbf4efb0fe71a3a61ba2a1c6ca240&amp;chksm=4308658f747fec99aa0f45a6813552f40bd56dc0a0a1d1ef502423aff0280d028fb47d52052d#rd",
    //             "thumb_url": "http://mmbiz.qpic.cn/mmbiz_jpg/CAkaCWmWxmLChhuVtv8CynBQRhQM1QZV4UTGUuchFXWlsuY0n0jlERjlJdUCeB6e14BDlVkKZHtxvBDbllgWzg/0?wx_fmt=jpeg"
    //         },
    //         {
    //             "id": "002",
    //             "title": "Python",
    //             "digest": "随着互联网的",
    //             "url": "http://mp.weixin.qq.com/",
    //             "thumb_url": "http://mmbiz.qpic.cn"
    //         }
    //     ]
    // };
};

module.exports = {
    'GET /getMovie': getMovie,
    // 'GET /getMovieList': getMovieList,
    'POST /addMovies': addMovies,
}
