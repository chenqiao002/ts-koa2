/**
 * 生成随机字符串
 * len: Number 字符串长度
 */
//  module.exports.randomString = function (len: number):string {
export const randomString = function (len: number):string {
    let lenght: number = len || 32;
    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
    let $chars: string = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678";
    let maxPos: number = $chars.length;
    let pwd = "";
    for (let i = 0; i < lenght; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
};

export const apiResult = function(data: any = "", code?: number, msg?: string ): object{
    let type = typeof data;
    let resData = data;
    if ("object" === type) {
        resData = JSON.parse(JSON.stringify(data));
    }
    return {
        code: code || 200,
        msg: msg || "",
        data: resData
    }
}