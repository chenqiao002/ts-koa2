/**
 * 打开浏览器
 */
import { exec } from 'child_process';

const openUrl = function (url: string) {
    switch (process.platform) {
        case 'win32':
            exec(`start ${url}`);
            break;
        case 'linux':
            exec(`xdg-open ${url}`);
            break;
        case 'darwin':
            exec(`open ${url}`);
            break;
        default:
            exec(`start ${url}`);
            break;
    }
};

export default openUrl;
module.exports = openUrl;