/**
 * mongodb collection 集合构造器
 */
// Create a new MongoClient
import Db from './db';
const MongoDb: Db = Db.getInstance();
const { ObjectID } = require('mongodb');
// mongodb 单例模式创建连接
class Collection {
    public collection: any;
    constructor(collectionName: string) {
        this.collection = MongoDb.dbClient.collection(collectionName)
    }

    // 查找操作 collection：表名 json：查找条件
    public find(json: any) {
        return new Promise((resolve, reject) => {
            let result = this.collection.find();
            result.toArray((err: any, docs: unknown) => {
                if (!err) {
                    resolve(docs);
                } else {
                    reject(err);
                }
            });
        });
    }

    // 新增操作
    public insert(json: any) {
        return new Promise((resolve, reject) => {
            this.collection.insertOne(json, (err: any, result: unknown) => {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        });
    }

    // 修改操作
    public update(json1: any, json2: any) {
        return new Promise((resolve, reject) => {
            this.collection.updateOne(json1, { $set: json2, }, (err: any, result: unknown) => {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        });
    }

    // 删除操作
    public delete(json: any) {
        return new Promise((resolve, reject) => {
            this.collection.removeOne(json, (err: any, result: unknown) => {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        });
    }

    // 在进行查询或者删除操作的时候，我们一般都会通过id去进行操作，但是我们直接使用传递过来的id是不起作用的，需要使用mongodb提供的ObjectID方法，生成一个新的id去查询。
    public getObjectID(id: any) {
        return new ObjectID(id);
    }
}
export default Collection;
module.exports = Collection;