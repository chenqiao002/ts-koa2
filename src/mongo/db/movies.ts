/**
 * movies 集合
 */
import Collection from '../collection';

class Movies extends Collection {

}

const movies = new Movies("movies");
export default {
    movies
};
module.exports = {
    movies
};