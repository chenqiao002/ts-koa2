/**
 * users 集合
 */
import Collection from '../collection';
type userName = {
    name: string
}

class Users extends Collection {
    public findByUserName(json: userName) {
        return this.collection.findOne(json);
    }
}

const users = new Users('users');

export default {
    users
};
module.exports = {
    users
};
