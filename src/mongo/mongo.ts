/**
 * mongodb 入口，自动导入所有集合并返回集合对象
 */
import fs from "fs";
import { rootDir } from "@root/config";

const dbsDir: string = `${rootDir}\\mongo\\db`;
const dbCollections: any = {};

function addMapping(mapping: any) {
    for (let collection in mapping) {
        if (collection) {
            dbCollections[collection] = mapping[collection];
            dbCollections[`${collection}Collection`] = mapping[collection].collection;
        }
    }
}

function addCollections() {
    let files = fs.readdirSync(dbsDir);
    let jsFiles = files.filter((file: string) => {
        return file.endsWith('.ts') || file.endsWith('.js');
    });
    for (let file of jsFiles) {
        let mapping = require(`${dbsDir}\\${file}`);
        addMapping(mapping);
    }
}

addCollections();
export default dbCollections;
module.exports = dbCollections;
