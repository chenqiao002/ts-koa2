/**
 * 项目配置文件
 */
export const rootDir = __dirname;
export const port = 3001;
export const apiPrefix = '/api';
export const mongoConfig = {
    url: 'mongodb://localhost:27017',
    dbName: 'Movie'
}


module.exports = {
    // 项目根目录
    rootDir: __dirname,
    // 服务启动端口
    port,
    // 服务路径前缀
    apiPrefix,
    // mongodb配置
    mongoConfig
}