/**
 * 项目入口
 */
import path from 'path';
import koa from 'koa';
import cors from 'koa2-cors';
import bodyParser from 'koa-body';
import compress from 'koa-compress';
import helmet from 'koa-helmet';
import creatRouter from '@router/router';
import koaStatic from 'koa-static';
// import openUrl from '@utils/openUrl';

import { port } from './config';
import { logger, apiLog, timeLog, apiNotFound, noPermission } from '@utils/middlewares';
import mongo from './mongo/db';
// require('./mongoose/mongoose');

// 静态json数据
// import json from "@root/data.json";
// console.log(json);

const publicPath = path.join(__dirname, './public');
const uploadPath = path.join(__dirname, './public/upload/');

const app = new koa();
const run = () => {
    const router = creatRouter();
    // 当数据超过2kb的时候，可以压缩
    const compressOpts = { threshold: 2048 };

    // 静态资源
    app.use(koaStatic(publicPath));

    // 跨域设置
    app.use(cors());

    app.use(compress(compressOpts));

    app.use(helmet());

    // 请求体解析(包含了formdata文件上传和json数据)
    app.use(bodyParser({
        // patchKoa: true,
        // json: true,
        // text: true,
        // urlencoded: true,
        // encoding: 'utf-8',
        multipart: true, // 支持文件上传
        formidable: {
            multiples: true,
            maxFileSize: 200 * 1024 * 1024,    // 设置上传文件大小最大200M (默认2M)
            // keepExtensions: true,    // 保持文件的后缀
            // uploadDir:uploadPath, // 设置文件上传目录
            // onFileBegin: function(name,file){ // 文件上传前的设置
            //     console.log(`onFileBegin name: ${name}`);
            //     // console.log(file);
            // },
        }
    }));

    // 自定义中间件
    if ("dev" === process.env.NODE_ENV) {
        // 使用日志中间件
        app.use(logger());
        app.use(apiLog);
    }

    app.use(timeLog);
    // 404
    app.use(apiNotFound);
    // 401
    app.use(noPermission);

    // 组装返回值必须与use router在一起
    // app.use(apiResult);

    // 启动路由
    app.use(router.routes());
    // 官方文档推荐用法,router.allowedMethods()用在了路由匹配router.routes()之后,在所有路由中间件最后调用,根据ctx.status设置response响应头,当请求出错时的处理逻辑
    app.use(router.allowedMethods());
    app.listen(port);

    // 在非生产环境下，自动打开浏览器访问服务
    // if ("dev" === process.env.NODE_ENV) {
    //     openUrl(`http://localhost:${port}/`)
    // }

    console.log(`app network: http://localhost:${port}/`);
    console.log(`api  server: http://localhost:${port}/api/`);
}
// mongodb 连接完成之后启动项目
mongo.getInstance(run);
