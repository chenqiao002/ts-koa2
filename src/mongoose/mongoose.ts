import mongoose from "mongoose";
import { mongoConfig } from '../config';
const { url, dbName } = mongoConfig;
const db = `${url}/${dbName}`;

// 如果有账户密码需要采用下面的连接方式： 
// mongoose.connect('mongodb://eggadmin:123456@localhost:27017/eggcms');

mongoose.set('debug', true)
mongoose.connect(db);

mongoose.connection.on("disconnected", () => {
    mongoose.connect(db);
});

mongoose.connection.on("error", (err) => {
    console.error('Connect Failed: ', err);
});

mongoose.connection.on("open", async () => {
    console.log('🚀 Connecting MongoDB Successfully 🚀');
});

export default mongoose;
module.exports = mongoose;