import mongoose from "../mongoose";
const MovieSchema = new mongoose.Schema({
    name: { type: String, trim: true },             // 影片名称
    years: { type: Number },                        // 上映年代
    director: [String],                             // 导演
    category: [String],                             // 影片类型
    comments: [                                     // 影评
        {
            author: String,
            createdAt: {
                type: Date,
                default: Date.now(),
            },
            updatedAt: {
                type: Date,
                default: Date.now()
            }
        }
    ],
});
const Movies = mongoose.model("Movies", MovieSchema, "movies");
export default Movies;
module.exports = Movies;