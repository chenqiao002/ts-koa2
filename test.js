console.log("start");

async function test() {
    console.log("test start");
    await new Promise(function(res, reg) {
        setTimeout(() => {
            console.log("promise timeout 1000");
            res();
        })
    }, 1000)
    console.log("test end");
}

test();

console.log("end");