## 项目介绍

- 项目名称：ts-koa2
- 项目源地址：[ts-koa2](https://gitee.com/cq136/ts-koa2)

&emsp;&emsp;本项目为 typescript+koa2+mongodb 开发的 app 后台服务。node 版本为[14.15.4](https://npm.taobao.org/mirrors/node/v14.15.4/)，项目编译使用[tsc+tsconfig.json](https://www.tslang.cn/docs/handbook/tsconfig-json.html)，项目打包使用[gulp](https://www.gulpjs.com.cn/)。各项运行命令介绍如下：

1. 运行 **npm install** 安装依赖。

2. 运行 **npm run dev** 启动开发环境。

3. 运行 **npm run build** 项目打包。

4. 打包完成后运行 **npm run dist** 运行生产包。

5. 将dist文件夹拷贝到服务器，进入 dist 运行 **npm run prod** 即可安装生产依赖和运行生产包。


## 目录

项目根目录为**./src**，所有的开发代码均在此文件夹下
src
|--app.ts                               入口文件
|--config.ts                           配置文件
|--router                               路由(api)文件夹
|   |--router.ts                      路由
|   |--routers                        路由管理文件夹
|        |--**.ts                        路由(api)文件
|--utils                                   工具文件夹
|--mongo                              mongodb配置文件夹


## 说明

1. process.env.NODE_ENV 当前运行环境，或者在 app.ts 里实例化 app 之后使用 app.env 也可以获得当前环境标示

```
// 当前环境
// console.log(app.env);
// console.log(process.env.NODE_ENV);
```

2. 可以使用 import 和 @ 别名，项目中同时支持commonjs require+module.exports 写法 和es6 import/export 的写法，打包时会自动转换，两种语法在开发可以同时支持，在src/router/routers/*.ts中默认都是使用 module.exports 写法，因为动态引入使用的是require，其实在最终 import 语法会自动转译成 require，在动态引入路由文件时建议使用同步的require，使用import可能会导致项目启动后路由还未加载完成的问题。

```javascript
// CommonJS
const { logger } = require("@root/utils/logger");
module.exports.logger = function (){
    console.log("logger")
}

// ES6 import/export
import { logger } from "@root/utils/logger";
export const logger = function (){
    console.log("logger")
}
```

3. koa-logger 的[安装使用](https://www.jianshu.com/p/758d85bb9439?utm_source=tuicool&utm_medium=referral)

```
// 安装
npm install koa-logger --save
// ts支持
npm install @types/koa-logger --save-dev

const Koa_Logger = require("koa-logger");                 // 日志中间件

// 实例化
const app = new Koa();
const logger = Koa_Logger((str) => {                // 使用日志中间件
    console.log(Moment().format('YYYY-MM-DD HH:mm:ss')+str);
});

// 使用中间件
app.use(logger);

```


4. mongodb 的[安装使用](https://www.cnblogs.com/wisewrong/p/13280266.html), mongodb在经过封装之后src/mongo/mongo.ts的导出为mongo/db文件夹下所有的collecion,通过引入mongo.ts即可使用所有的collection原生api,以下三种使用方式均可，建议最后一种，项目启动
```
// import db from '@mongo/mongo';
const { usersCollection } = require('@mongo/mongo')

const getUsers = async (ctx: any) => {
    // const result = await db.users.find({}); // 查询
    // db.users.collection.find().toArray((err: any, docs: unknown) => {
    // 	if (!err) {
    // 		console.log('docs', docs);
    // 	} else {
    // 		console.log('err', err);
    // 	}
    // });
    // db.usersCollection.find().toArray((err: any, docs: unknown) => {
    // 	if (!err) {
    // 		console.log('docs', docs);next()
    // 	} else {
    // 		console.log('err', err);
    // 	}
    // });

    const res = await new Promise((resolve, reject) => {
        usersCollection.find().toArray((err: any, docs: unknown) => {
            if (!err) {
                resolve(docs);
            } else {
                reject(err);
            }
        });
    });

    ctx.body = res;
};
module.exports = {
    'GET /getUsers': getUsers,
};


```

5. 项目打包可以使用npm run build 和npm run gulp 两者打包稍微有点区别，build打包依赖于 rm -rf 命令，但是打包结果是最小的压缩版，npm run gulp 则是不依赖 rm -rf ，但是打包结果包含了tsc转义后的文件 dist/tsc 可以手动删除该文件夹以达到和 build相同的结果，当build命令不能使用时可以使用gulp打包



## 版权和许可信息

app-server 经过 Apache License, version 2.0 授权许可。
